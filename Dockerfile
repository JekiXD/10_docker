FROM node:latest as build
WORKDIR /app
COPY package.json package.json
RUN yarn install
COPY . .
RUN yarn build


FROM nginx:1.19-alpine
COPY --from=build /app/dist /opt/dist
COPY --from=build /app/index.html /opt/index.html
COPY --from=build /app/resources /opt/resources
COPY nginx/nginx.conf /etc/nginx/nginx.conf
