import { FighterDetails, fightersDetails } from "./helpers/mockData";

export function fight(firstFighter : FighterDetails, secondFighter : FighterDetails) : FighterDetails {
  let winner : FighterDetails;
  while(true)
  {

      firstFighter.health -= getDamage(secondFighter, firstFighter);
      console.log(`${firstFighter.name} health now is ${firstFighter.health}`);
      if(firstFighter.health <= 0) {winner = secondFighter;  break;}

      secondFighter.health -= getDamage(firstFighter, secondFighter);
      console.log(`${secondFighter.name} health now is ${secondFighter.health}`);
      if(secondFighter.health <= 0) {winner = firstFighter;  break;}
      console.log("\n");
  }

  console.log("----Fight is finished----");

  return winner;
}

export function getDamage(attacker : FighterDetails, enemy : FighterDetails) : number {
  let damage : number = getHitPower(attacker) - getBlockPower(enemy);
  damage = damage < 0 ? 0 : damage;
  console.log(`${attacker.name} punched ${enemy.name} with ${damage} damage`);
  return damage 
}

export function getHitPower(fighter : FighterDetails) : number {
  let criticalHitChance : number = Math.floor(Math.random() * 2) + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter : FighterDetails) : number {
  let dodgeChance : number = Math.floor(Math.random() * 2) + 1;
  return fighter.defense * dodgeChance;
}
