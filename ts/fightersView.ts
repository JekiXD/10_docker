import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter, FighterDetails } from './helpers/mockData';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters : (Fighter | undefined)[]) : HTMLElement{
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters
                          .filter(fighter => fighter)
                          .map(fighter => createFighter(fighter!, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event : Event, fighter : Fighter) : Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  if(fullInfo) showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) : Promise<FighterDetails | undefined> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector() : (event: Event, fighter: Fighter) => Promise<void> {
  const selectedFighters = new Map<string, FighterDetails>();

  return async function selectFighterForBattle(event : Event, fighter : Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo!);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const it = selectedFighters.values();
      const winner = fight(Object.assign({}, it.next().value) , Object.assign({}, it.next().value));
      showWinnerModal(winner);
    }
  }
}
