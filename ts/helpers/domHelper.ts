export interface IHtmlElement{
  tagName: string,
  className?: string,
  attributes?: {[index : string] : string}
}

export function createElement(htmlElement: IHtmlElement): HTMLElement {
  const element = document.createElement(htmlElement.tagName);
  
  if (htmlElement.className) {
    element.classList.add(htmlElement.className);
  }

  if(htmlElement.attributes) {
    Object.keys(htmlElement.attributes).forEach(key => element.setAttribute(key, htmlElement.attributes ? htmlElement.attributes[key] : ""));
  }

  return element;
}