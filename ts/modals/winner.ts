import { createElement } from "../helpers/domHelper";
import { FighterDetails } from "../helpers/mockData";
import { showModal } from './modal';

export  function showWinnerModal(fighter : FighterDetails) : void {
  // show winner name and image
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter : FighterDetails) : HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const fighterinfo = createElement({ tagName: 'div', className: 'fighter-info' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src : fighter.source}});

  nameElement.innerText = fighter.name;

  fighterinfo.append(nameElement, imageElement)
  fighterDetails.append(fighterinfo);

  return fighterDetails;
}