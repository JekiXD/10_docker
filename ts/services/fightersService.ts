import { callApi } from '../helpers/apiHelper';
import { Fighter, FighterDetails } from '../helpers/mockData';

export async function getFighters() : Promise<(Fighter | undefined)[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id : string) : Promise<FighterDetails | undefined> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult[0];
  } catch (error) {
    throw error;
  }
}

